import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:netgi_app/components/widgets/buttons/styled_button.dart';
import 'package:netgi_app/components/widgets/gestureDetector/styled_gesture_detector.dart';
import 'package:netgi_app/components/widgets/textfield/styled_text_field.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 130),
                  child: Text(
                    "NetGi",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 64,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const SizedBox(height: 72),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const StyledTextField(
                        label: 'Digite sua Matrícula ou CPF',
                        size: 40,
                      ),
                      const SizedBox(height: 27),
                      const StyledTextField(label: 'Senha', size: 127),
                      const SizedBox(height: 10),
                      GestureDetector(
                        onTap: () {
                          context.go('/recuperarSenha');
                        },
                        child: const Padding(
                          padding: EdgeInsets.only(left: 170),
                          child: Text(
                            'Esqueceu a senha?',
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                      const StyledGestureDetector(
                          route: 'cadastro',
                          blueText: 'Ainda não tem conta? ',
                          orangeText: 'Crie agora')
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                const Padding(
                    padding: EdgeInsets.only(bottom: 170),
                    child: StyledButton(
                      btnLabel: 'ENTRAR',
                      route: 'homePage',
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

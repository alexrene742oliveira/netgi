import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:netgi_app/components/widgets/buttons/styled_button.dart';
import 'package:netgi_app/components/widgets/gestureDetector/styled_gesture_detector.dart';

class RecuperarSenhaConfirmacao extends StatelessWidget {
  const RecuperarSenhaConfirmacao({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Center(
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Text(
                    'NetGi',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
                const SizedBox(height: 70),
                SizedBox(
                  width: 118,
                  height: 118,
                  child: Image.asset('assets/images/senhaAsterisco.png'),
                ),
                const SizedBox(height: 50),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Foi encaminhado um link de recuperação de senha para o email',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 28,
                      color: Colors.blue,
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                const Text(
                  'vic*******16@gmail.com',
                  style: TextStyle(
                    fontSize: 28,
                    color: Colors.blue,
                  ),
                ),
                const SizedBox(height: 140),
                const StyledButton(btnLabel: 'REENVIAR LINK', route: ''),
                const StyledGestureDetector(
                    route: 'login',
                    blueText: 'Recuperou sua senha? ',
                    orangeText: 'Fazer login'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:netgi_app/components/widgets/buttons/styled_button.dart';
import 'package:netgi_app/components/widgets/gestureDetector/styled_gesture_detector.dart';
import 'package:netgi_app/components/widgets/textfield/styled_text_field.dart';

class RecuperarSenha extends StatelessWidget {
  const RecuperarSenha({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Recuperar Senha',
      home: Scaffold(
        body: SafeArea(
          child: Center(
            child: Column(
              children: const [
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Text(
                    'NetGi',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                SizedBox(height: 60),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Informe o E-mail cadastrado para enviarmos as instruções de redefinição de senha',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 28,
                      color: Colors.blue,
                    ),
                  ),
                ),
                SizedBox(height: 150),
                StyledTextField(label: 'XXXXXX@gmail.com', size: 75),
                SizedBox(height: 150),
                StyledButton(
                    btnLabel: 'ENVIAR LINK',
                    route: 'recuperarSenhaConfirmacao'),
                StyledGestureDetector(
                    route: 'login',
                    blueText: 'Recuperou sua senha? ',
                    orangeText: 'Fazer login'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:go_router/go_router.dart';
import 'package:netgi_app/screen/apresentacao.dart';
import 'package:netgi_app/screen/home_page.dart';
import 'package:netgi_app/screen/login.dart';
import 'package:netgi_app/screen/recuperar_senha.dart';
import 'package:netgi_app/screen/tela_confirmacao_recuperacao_senha.dart';

final routes = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const ImageSlider(),
    ),
    GoRoute(
      path: '/login',
      builder: (context, state) => const LoginPage(),
    ),
    GoRoute(
      path: '/recuperarSenha',
      builder: (context, state) => const RecuperarSenha(),
    ),
    GoRoute(
      path: '/recuperarSenhaConfirmacao',
      builder: (context, state) => const RecuperarSenhaConfirmacao(),
    ),
    GoRoute(
      path: '/homePage',
      builder: (context, state) => const HomePage(),
    ),
  ],
);

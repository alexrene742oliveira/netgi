import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class StyledButton extends StatelessWidget {
  final String btnLabel;
  final String route;

  const StyledButton({super.key, required this.btnLabel, required this.route});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        context.go('/$route');
      },
      style: ElevatedButton.styleFrom(
        padding: const EdgeInsets.symmetric(horizontal: 54),
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        minimumSize: const Size(252, 60),
      ),
      child: Text(
        btnLabel,
        style: const TextStyle(
          fontSize: 25,
        ),
      ),
    );
  }
}

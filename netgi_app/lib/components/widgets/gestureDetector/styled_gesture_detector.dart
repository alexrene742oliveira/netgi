import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class StyledGestureDetector extends StatelessWidget {
  final String route;
  final String blueText;
  final String orangeText;

  const StyledGestureDetector(
      {super.key,
      required this.route,
      required this.blueText,
      required this.orangeText});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.go('/$route');
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 45),
        child: RichText(
          text: TextSpan(
            style: const TextStyle(fontSize: 16),
            children: <TextSpan>[
              TextSpan(
                text: blueText,
                style: const TextStyle(color: Colors.blue),
              ),
              TextSpan(
                  text: orangeText,
                  style: const TextStyle(color: Color(0XFFF96815))),
            ],
          ),
        ),
      ),
    );
  }
}

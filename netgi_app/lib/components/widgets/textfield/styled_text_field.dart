import 'package:flutter/material.dart';

class StyledTextField extends StatelessWidget {
  final String label;
  final double size;

  const StyledTextField({super.key, required this.label, required this.size});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 300,
      child: Align(
        alignment: Alignment.center,
        child: TextField(
          decoration: InputDecoration(
            labelText: label,
            contentPadding: EdgeInsets.only(left: size),
            border: OutlineInputBorder(
              borderSide: const BorderSide(width: 5.0),
              borderRadius: BorderRadius.circular(20),
            ),
            labelStyle: const TextStyle(
              fontSize: 16,
              color: Colors.grey,
              height: 1.5,
              letterSpacing: 0.5,
              wordSpacing: 2.0,
            ),
          ),
        ),
      ),
    );
  }
}
